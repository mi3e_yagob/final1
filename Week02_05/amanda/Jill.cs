﻿using UnityEngine;
using System.Collections;

public class Jill : MonoBehaviour {
    public Animator MyJill;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.W)){
            MyJill.SetFloat("speed", 1);
        }
        else{
            MyJill.SetFloat("speed", 0);
        }

        if (Input.GetKeyDown(KeyCode.LeftControl)){
            MyJill.SetBool("roll", true);
        }
        else {
            MyJill.SetBool("roll", false);
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            MyJill.SetBool("crouch", true);
        }
        else {
            MyJill.SetBool("crouch", false);
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            MyJill.SetBool("walk", true);
        }
        else {
            MyJill.SetBool("walk", false);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MyJill.SetBool("jump", true);
        }
        else {
            MyJill.SetBool("jump", false);
        }

    }
}
