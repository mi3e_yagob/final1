﻿using UnityEngine;
using System.Collections;

public class State : MonoBehaviour {

	private float lifeTime;

	// Use this for initialization
	void Start () {
		lifeTime = 2.5f;
	}
	
	// Update is called once per frame
	void Update () {
		lifeTime -= Time.deltaTime;
		if (lifeTime <= 0) {
			Destroy (this.gameObject);
		}
		transform.Translate (Vector3.down * 5 * Time.deltaTime);
	
	}
}
