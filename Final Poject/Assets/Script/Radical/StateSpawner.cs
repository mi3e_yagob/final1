﻿using UnityEngine;
using System.Collections;

public class StateSpawner : MonoBehaviour {

	public int Score;

	public GameObject prefabState;

	public float dropTime;
	public float dropTimer;

	public float randomX;

	public bool isGameEnd;

	private Player getPlayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isGameEnd) {
			dropTimer += Time.fixedDeltaTime;
			if (dropTimer >= dropTime) {
				dropTime = Random.Range (0.5f, 2.0f);
				randomX = Random.Range (-3.5f, 3.5f);
				Vector3 pos = new Vector3 (randomX, 5, 0);
				Instantiate (prefabState, pos, Quaternion.identity);
				dropTimer = 0;
				getPlayer = FindObjectOfType<Player>();
				if(getPlayer == null)
				{
					isGameEnd = true;
				}
			}
		}
	}
}
