﻿using UnityEngine;
using System.Collections;

public class Pass : MonoBehaviour {

	private StateSpawner getStateSpawner;

	// Use this for initialization
	void Start () {
		getStateSpawner = GameObject.FindObjectOfType<StateSpawner> ();
	
	}
	void OnCollisionEnter2D(Collision2D other) {
		Destroy (this.gameObject);
		getStateSpawner.Score++;
		Debug.Log("Pass!!");
	}
}
