﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public KeyCode leftKey;
	public KeyCode rightKey;
	public float Speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(leftKey))
		{
			this.gameObject.transform.Translate(Vector3.left*Speed*Time.deltaTime);
		}
		if(Input.GetKey(rightKey))
		{
			this.gameObject.transform.Translate(Vector3.right*Speed*Time.deltaTime);
		}
	}
}
