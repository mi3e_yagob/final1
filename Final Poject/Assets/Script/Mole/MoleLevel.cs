﻿using UnityEngine;
using System.Collections;

public class MoleLevel : MonoBehaviour {

	public int score;

	public float Timer;
	public GameObject[] Markers;
	public GameObject Mole;

	private bool isEndgame;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!isEndgame) {
			Timer += Time.deltaTime;
			if (Timer >= 2f) {
				MoveMole (false);
			}
		}

		if (score >= 10) {
			isEndgame = true;
			Mole.SetActive(false);
		}
	}

	public void MoveMole(bool b)
	{
		int n = Random.Range (0, Markers.Length);
		Vector3 pos = Markers[n].transform.position;
		Mole.transform.position = pos;
		Timer = 0;

		if (b) {
			score++;
		}
	}
}
