﻿using UnityEngine;
using System.Collections;

public class Mole : MonoBehaviour {

	private MoleLevel getMolelevel;

	void Start()
	{
		getMolelevel = transform.parent.GetComponent<MoleLevel> ();
	}

	void OnMouseDown()
	{
		getMolelevel.MoveMole(true);
	}

}
