﻿using UnityEngine;
using System.Collections;

public class wasd : MonoBehaviour {

	public float Speed;
	public float TotalTime;
	public bool Star = false;

	/*void Update () {

	}*/
	
	void Update() 
	{
		if (Input.GetKey(KeyCode.Space)) 
		{
			Star = true;
		}
		if (Star) 
		{
			TotalTime += Time.deltaTime; 
			
			Vector3 position = transform.position;
			if (TotalTime < 2) {
				position.y += 0.06f;
				position.x += 0.02f;
			} else if (TotalTime < 4) {
				position.y -= 0.06f;
				position.x += 0.02f;
			} else if (TotalTime < 6) {
				position.y += 0.04f;
				position.x -= 0.08f;
			} else if (TotalTime < 8) {
				position.x += 0.10f;
			} else if (TotalTime < 10) {
				position.y -= 0.04f;
				position.x -= 0.08f;
			}
			if (TotalTime > 10) {
				Star = false;
			}
			transform.position = position;
		} 
		else 
		{
			float translation = Input.GetAxis("Vertical") * Speed;
			float rotation = Input.GetAxis("Horizontal") * Speed;
			translation *= Time.deltaTime;
			rotation *= Time.deltaTime;
			transform.Translate (rotation, translation, 0);
			TotalTime = 0.0f;
		}
	}
}

