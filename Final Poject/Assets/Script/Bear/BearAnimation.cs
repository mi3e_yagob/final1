﻿using UnityEngine;
using System.Collections;

public class BearAnimation : MonoBehaviour {

	private Animator getAnimator;

	// Use this for initialization
	void Start () {
		getAnimator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey (KeyCode.A)) 
		{
			getAnimator.SetInteger ("Animation", 1);
		} 
		else if (Input.GetKey (KeyCode.S)) 
		{
			getAnimator.SetInteger ("Animation", 2);
		}
		else if (Input.GetKey (KeyCode.D)) 
		{
			getAnimator.SetInteger ("Animation", 3);
		} 
		else 
		{
			getAnimator.SetInteger ("Animation", 0);
		}
	
	}
}
