﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour {

	public int score;
	public Spawner Spawner00;
	public Spawner Spawner01;
	public Spawner Spawner02;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (score >= 5) {
			Debug.Log ("Winner");
			Spawner00.EndGame();
			Spawner01.EndGame();
			Spawner02.EndGame();
		} else if (score < 0) {
			Debug.Log ("Lose");
			Spawner00.EndGame();
			Spawner01.EndGame();
			Spawner02.EndGame();
		}
		
	
	}

	public void UpdateScore(int i)
	{
		score += i;
	}
}
