﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject GoodItemPrefab;
	public GameObject BadItemPrefab;
	public GameObject[] Markers;
	public float Timer;
	public GameObject Mole;
	public float Droptimer;
	bool isEndgame = false;

	// Update is called once per frame
	void Update () {
		if (!isEndgame) {
			Timer += Time.deltaTime;
			if (Timer >= Droptimer) {
				int n = Random.Range (0, 10);
				if (n <= 7) {
					Instantiate (GoodItemPrefab, transform.position,
				            Quaternion.identity);
				} else {
					Instantiate (BadItemPrefab, transform.position,
					            Quaternion.identity);
				}
				Droptimer = Random.Range (2.0f, 4.9f);
				Timer = 0;
			}
		}
	}
	public void EndGame ()
	{
		isEndgame = true;
	}
}



