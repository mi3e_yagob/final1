﻿using UnityEngine;
using System.Collections;

public class BaseEnemy : MonoBehaviour {

	public int HitPoint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Bullet")
		{
			GetDamage();
		}
	}

	private void GetDamage()
	{
		HitPoint -= 1;
		if(HitPoint <= 0)
			Destroy(this.gameObject);
	}
}
