﻿using UnityEngine;
using System.Collections;

public class BasePlayer : MonoBehaviour {

	public KeyCode leftKey;
	public KeyCode rightKey;
	public KeyCode fireKey;

	public GameObject prefabBullet;

	public float coolDown;
	private float coolDownTimmer;
	private bool isReadyFire = false;
	
	void Update () 
	{
		if(!isReadyFire)
		{
			coolDownTimmer += Time.deltaTime;
			if(coolDownTimmer >= coolDown)
			{
				isReadyFire = true;
				coolDownTimmer = 0f;
			}
		}
		if(Input.GetKey(leftKey))
		{
			this.gameObject.transform.Translate(Vector3.left*5*Time.deltaTime);
		}
		if(Input.GetKey(rightKey))
		{
			this.gameObject.transform.Translate(Vector3.right*5*Time.deltaTime);
		}
		if(Input.GetKeyDown(fireKey))
		{
			if(isReadyFire)
			{
				Debug.Log ("Fire!!!");
				Instantiate(prefabBullet,transform.position,Quaternion.identity);
				isReadyFire = false;
			}
		}
	}
}
