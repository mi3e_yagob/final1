﻿using UnityEngine;
using System.Collections;

public class MyG : MonoBehaviour {
    public Animator MyG1;
    public int MyHP;

    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.W))
        {
            MyG1.SetFloat("Speed", 1);
        }
        else {
            MyG1.SetFloat("Speed", 0);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            MyG1.SetBool("IsSprint", true);
        }
        else {
            MyG1.SetBool("IsSprint", false);
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            MyG1.SetBool("Iscrouch", true);
        }
        else {
            MyG1.SetBool("Iscrouch", false);
            }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //SkeltonAnimator.SetInteger("MyHP", 0);
            MyHP = 0;
        }

        //if (Input.GetKey(KeyCode.Space))
        //{
        //    Mybot.SetBool("Jump", true);
        //}
        //else {
        //    Mybot.SetBool("Jump", false);
        //}

        if (Input.GetKeyDown(KeyCode.D))
        {
            MyG1.SetTrigger("Attack1");
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            MyG1.SetTrigger("Attack2");
        }

    }
}
