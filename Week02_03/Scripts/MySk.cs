﻿using UnityEngine;
using System.Collections;

public class MySk : MonoBehaviour {

    public Animator MySsk;
    public int MyHP;

    // Use this for initialization
    void Start () {
        MyHP = 100;


    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.W))
        {
            MySsk.SetFloat("Speed", 1);
        }
        else {
            MySsk.SetFloat("Speed", 0);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            MySsk.SetBool("IsSprint", true);
        }
        else {
            MySsk.SetBool("IsSprint", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //SkeltonAnimator.SetInteger("MyHP", 0);
            MyHP = 0;
        }

        //if (Input.GetKey(KeyCode.Space))
        //{
        //    Mybot.SetBool("Jump", true);
        //}
        //else {
        //    Mybot.SetBool("Jump", false);
        //}

        if (Input.GetKeyDown(KeyCode.D))
        {
            MySsk.SetTrigger("Attack1");
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            MySsk.SetTrigger("Attack2");
        }
    }
}
