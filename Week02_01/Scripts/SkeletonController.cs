﻿using UnityEngine;
using System.Collections;

namespace IGD246.Week02 {

    public class SkeletonController : MonoBehaviour {

		public Animator SkeltonAnimator;
		public int MyHP;

        // Use this for initialization
        void Start() {
			MyHP = 100;

        }

        // Update is called once per frame
        void Update() {
			if (Input.GetKey (KeyCode.W)) {
				SkeltonAnimator.SetFloat ("Speed", 1);
			} else {
				SkeltonAnimator.SetFloat ("Speed", 0);
			}

			if (Input.GetKey (KeyCode.LeftShift)) {
				SkeltonAnimator.SetBool ("IsSprint", true);
			} else {
				SkeltonAnimator.SetBool ("IsSprint", false);
			}

			if (Input.GetKeyDown (KeyCode.Space)) {
                //SkeltonAnimator.SetInteger("MyHP", 0);
                MyHP = 0;
			}

			if (Input.GetKeyDown (KeyCode.D)) {
				SkeltonAnimator.SetTrigger ("Attack Trigger");
			}

//			SkeltonAnimator.SetInteger ("HP", MyHP);
//
//			if (Input.GetKeyDown (KeyCode.D)) {
//				SkeltonAnimator.SetBool ("Attack", true);
//			}
//			if (Input.GetKeyUp (KeyCode.D)) {
//				SkeltonAnimator.SetBool ("Attack", false);
//			}

        }
    }

}